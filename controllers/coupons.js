'use strict';

module.exports = function (app, router) {

    router.get('/coupons/:code', function (req, res, next) {

        var couponService = require('../services/coupons')(app);

        var couponCode = req.params.code || 0;

        couponService.getByCode(couponCode, function (result) {
            var data = result[0];
            var user = app.authUser;

            var coupon = {};

            if (data === undefined || data.length == 0 || data.id == null) {
                coupon = { status: 0 }
            } else {
                coupon = {
                    id: data.id,
                    code: data.code,
                    units: data.quantity,
                    paymentMethod: data.paymentMethod,
                    buyDate: data.dateCreated,
                    expirationDate: data.dateFinish,
                    lastType: data.lastType,
                    expired: data.expired ? true : false,
                    user: {
                        id: data.user_id,
                        name: data.user_name,
                        photo: data.user_photo
                    },
                    offer: {
                        name: data.offer_name,
                        new_value: app.helpers.format(data.new_value, user.currency),
                        old_value: app.helpers.format(data.old_value, user.currency),
                        bought: data.bought,
                        redeemed: data.redeemed
                    },
                    payment: {
                        credit: (data.payByCreditsAmount > 0) ? app.helpers.format(data.payByCreditsAmount,user.currency) : data.payByCreditsAmount,
                        store: (data.payByDirectEstablishmentAmount > 0) ? app.helpers.format(data.payByDirectEstablishmentAmount,user.currency) : data.payByDirectEstablishmentAmount,
                        card: (data.payByCreditCardAmount > 0) ? app.helpers.format(data.payByCreditCardAmount,user.currency) : data.payByCreditCardAmount
                    },
                    status: 1
                };
            }

            res.json(coupon);
        });
    });

    router.get('/coupons/details/:code', function (req, res, next) {
        var couponService = require('../services/coupons')(app);

        var couponCode = req.params.code || 0;

        couponService.getInfoByCode(couponCode, function (result) {
            var data = result[0];
            var user = app.authUser;

            var coupon = {};

            console.log("\n\naqui\n\n");

            if (data === undefined || data.length == 0 || data.id == null) {
                coupon = { status: 0 }
            } else {
                coupon = {
                    id: data.id,
                    code: data.code,
                    units: data.quantity,
                    paymentMethod: data.paymentMethod,
                    buyDate: data.dateCreated,
                    expirationDate: data.dateFinish,
                    lastType: data.lastType,
                    expired: data.expired ? true : false,
                    user: {
                        id: data.user_id,
                        name: data.user_name,
                        photo: data.user_photo
                    },
                    offer: {
                        id: data.offer_id,
                        name: data.offer_name,
                        description: data.offer_description,
                        new_value: app.helpers.format(data.new_value, user.currency),
                        old_value: app.helpers.format(data.old_value, user.currency),
                        bought: data.bought,
                        redeemed: data.redeemed
                    },
                    payment: {
                        credit: data.payByCreditsAmount,
                        store: data.payByDirectEstablishmentAmount,
                        card: data.payByCreditCardAmount
                    },
                    status: 1
                };


                // Callback to insert observation into coupon object.
                var insertObservations = function(result) {
                    var observations = [];
                    var i;
                    for (i in result) {
                        var observation = {
                            title: result[i].title,
                            content: result[i].description
                        };
                        observations.push(observation);
                    }
                    coupon.observations = observations;
                    res.json(coupon);
                };

                // Callback to insert images into coupon object.
                var insertImages = function(result) {
                    var images = [];
                    var i;
                    for (i in result) {
                        var image = {
                            order: result[i].order,
                            photo: result[i].image
                        };
                        images.push(image);
                    }
                    coupon.images = images;

                    couponService.getObservationsByOfferId(coupon.offer.id, insertObservations);
                };

                couponService.getImagesByOfferId(coupon.offer.id, insertImages);
            }
        });
    });

    router.post('/coupons/redeem', function (req, res, next) {
        var couponService = require('../services/coupons')(app);
        var request = require('request');
        var couponCode = req.body.code || 0;

        var insertRedeemTransaction = function(result) {
            var coupon = result[0];
            if (coupon) {
                switch (coupon.type) {
                    case 1:
                    case 2:
                        couponService.createRedeemTransactionById(coupon.id, function (result) {
                            if (coupon.store && coupon.user) {
                                console.log('Sending POST to ' + app.mobileApi.url + '/' + app.mobileApi.version + '/establishments/send-inbox-evaluation \n');

                                /* Send notification to mobile user */
                                request.post(app.mobileApi.url + '/' + app.mobileApi.version + '/establishments/send-inbox-evaluation', {form:{establishmentId:coupon.store, userId: coupon.user, couponId: coupon.id}});

                                /* Send email to adviser */
                                request.post(app.mobileApi.url + '/' + app.mobileApi.version + '/market-mails/request-send', {form:{type:'coupon-validation-to-adviser', couponId: coupon.id}});

                                /* Send email to commerce */
                                request.post(app.mobileApi.url + '/' + app.mobileApi.version + '/market-mails/request-send', {form:{type:'validation-coupon-notify-commerce', couponId: coupon.id, userName: app.authUser.username}});
                            }
                            res.json({status:1});
                        });
                        break;
                    case 3: res.json({status:2});
                        break;
                    case 4: res.json({status:3});
                        break;
                    case 5: res.json({status:4});
                        break;
                    default:
                        res.json({status:5});
                }
            } else {
                res.json({status:0});
            }
        };

        couponService.getCouponStatus(couponCode, insertRedeemTransaction);
    });
};