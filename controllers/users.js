'use strict';

module.exports = function (app, router) {

    router.post('/users/login', function (req, res, next) {

        var userService = require('../services/users')(app);
        // var userVO = app.helpers.parseJson(req.body.userVO);
        var email = req.body.email;
        var password = req.body.password;
        var language = undefined;

        var params = {
            email: email || '',
            password: password || '',
            language: language || 'es'
        };

        userService.login(params, function (result) {
            var resp = {};

            if (result.length) {

                var data  = result[0];

                userService.updateLoginInfo(data.id, params.language);
                resp = {
                    'status': 1,
                    'id': data.id,
                    'name': data.name,
                    user: {
                        id: data.user_id,
                        name: data.user_name,
                        language: data.language,
                        email: data.email,
                        accessToken: data.accessToken
                    },
                    'address': data.address,
                    'lat': data.lat || 0,
                    'lng': data.lng || 0,
                    'currency': data.currency
                };
            } else {

                resp.status = 0;
            }

            res.json(resp);
        });
    });

    router.get('/users/validate/:token', function (req, res, next) {
        var userAuthorization = require('../services/userAuthorization')(app);

        userAuthorization.getUserByToken(req.params.token, function (user) {
            if (user) {
                res.json({status:1});
            } else {
                res.json({status:0});
            }
        });
    });

    router.get('/users/sold', function (req, res, next){
        var userService = require('../services/users')(app);
        var user = app.authUser;

        userService.getTotalSold(function (total) {
            var json = { status: 0 };
            if (total) {
                json = {
                    status:1,
                    total:app.helpers.format(total[0].value, user.currency)
                };
            }
            res.json(json);
        });
    });

    router.get('/users/reach', function (req, res, next) {
        var userService = require('../services/users')(app);
        var user = app.authUser;

        userService.getTotalNearbyUsersByCoordinates(user.lat, user.lng, 5, function (result) {
            if (result) {
                result.status = 1;
                res.json(result);
                return;
            }
            res.json({
                total:0,
                status:0
            });
        });
    });

    router.post('/users/remember', function (req, res, next){
        var userService = require('../services/users')(app);

        var email = req.body.email;

        userService.recoverPassword(email, function (result) {
            var json = { status: 0 };
            if (result) {
                json = result;
            }
            res.json(json);
        });
    });

    router.get('/users/adviser', function (req, res, next) {
        var userService = require('../services/users')(app);
        var resp = {};

        userService.getAdviserInfo(function(result) {
            if (result.length) {

                var data  = result[0];

                resp = {
                    status: 1,
                    id: data.id,
                    name: data.name,
                    phone: data.phone,
                    email: data.email,
                    photo: data.photo
                };
            } else {

                resp.status = 0;
            }

            res.json(resp);
        });
    });

    // default route
    router.get('/users/:id', function (req, res, next) {

        var userService = require('../services/users')(app);

        var userId = req.params.id || 0;

        userService.getById(userId, function (user) {

            res.json({result: user});
        });
    });
};