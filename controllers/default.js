'use strict';

module.exports = function (app, router) {

    // default route
    router.get('/', function (req, res, next) {

        res.json({message: 'What are you doing here?'});
    });
};