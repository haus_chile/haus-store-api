'use strict';

module.exports = function (app) {

    return {
        login: function (params, callback) {

            var email = params.email || '';
            var password = params.password || '';

            // prevent blank credentials
            if (email.trim() == '' || password.trim() == '') {

                callback([]);

                return false;
            }

            var sql = "SELECT  \
                    store.id, \
                    store.name, \
                    store.description, \
                    place.name address, \
                    user.id as user_id, \
                    user.name as user_name, \
                    user.email, \
                    user.accessToken, \
                    user.language, \
                    store.lat, \
                    store.lng, \
                    store.currency \
                FROM \
                    hausdb.market_users user \
                    INNER JOIN\
                    hausdb.establishments store \
                    ON user.establishmentId = store.id \
                    LEFT JOIN \
                    hausdb.main_places place \
                    ON store.placeId = place.id \
                WHERE \
                    user.email = ? AND user.inactive = 0 \
                        AND ((TRIM(user.password) <> '' \
                        AND TRIM(user.password) = ?)) \
                LIMIT 1;";

            app.db.query(sql, [email.trim(), password.trim()], function(err, result) {
                /**
                 * Update user access token
                 */
                if (result.length && (result[0].accessToken == '' || result[0].accessToken == null)) {
                    var userAuthService = require('../services/userAuthorization')(app);

                    result[0].accessToken = userAuthService.setUserToken(result[0].user_id);
                }

                callback(result);

            }, {cache: false});
        },
        updateLoginInfo: function(id, language) {

            var fields = [];

            if (language != undefined && language.trim() != '') {

                fields.push("language = '"+language+"'");
            }

            fields.push("dateAccess = now()");

            fields = fields.join(',');

            var sql = "UPDATE market_users SET "+fields+" WHERE id = ?;";

            app.db.query(sql, [id], function (err, response) {
                if(err) throw err;
            });
        },
        recoverPassword: function (email, callback) {

            var status = {
                notFound: 2,
                sent: 1,
                notSent: 0
            };

            if (email == undefined || email.trim() == '') {

                callback({
                    status: status.notFound
                });

                return;
            }

            email = email.trim().toLowerCase();

            var pattern = '0123456789abcdefghijklmnopqrstuvwxyz';

            var newPassword = app.helpers.shuffleString(pattern).substring(0, 6);

            var sql = "SELECT id, name, email, language \n\
            FROM market_users \n\
            WHERE TRIM(LOWER(email)) = ? \n\
            AND inactive = 0 \n\
            LIMIT 1;";


            app.db.query(sql, [email], function (err, result) {

                if (!result.length) {

                    callback({status: status.notFound});

                    return;
                }

                var user = result[0];
                var userName = user.name;
                var language = user.language || 'es';
                var translate = app.i18n();
                var mail = require('../components/sendMail')(app);

                translate.setLang(language);

                var subject = translate.t("Tu contraseña de Haus");

                var params = {
                    'tx01': translate.t("Estimado "),
                    'tx02': translate.t("Su nueva contraseña es: "),
                    'tx03': translate.t("Atentamente,"),
                    'tx04': translate.t("El Equipo de Haus"),
                    'name': userName,
                    'password': newPassword,
                    'subject': subject
                };

                app.db.query("UPDATE market_users SET password = ? WHERE id = ?;", [newPassword, user.id], function (err, result) {

                    if (err || result.affectedRows == 0) {

                        callback({status: status.notSent});

                        return;
                    }

                    mail.sendHtml(subject, email, 'password-recover/mail', params);

                    callback({status: status.sent});
                });

            }, {cache: false});

        },
        getById: function (userId, callback) {

            app.db.query("SELECT * FROM establishments WHERE id = ? LIMIT 1", [userId], function (err, result) {

                if (err) {

                    callback([]);

                    return;
                }

                callback(result);
            });
        },
        getTotalSold: function (callback) {
            var user = app.authUser;
            var sql = "SELECT sum((of.price * (1 - of.discount / 100)) * cp.quantity) AS value \n\
            FROM coupon_transactions cpt \n\
            INNER JOIN ( \n\
                SELECT couponId, max(date) maxDate \n\
            FROM coupon_transactions \n\
            GROUP BY couponId \n\
            ) cpt2 \n\
            ON cpt.couponId = cpt2.couponId \n\
            AND cpt.date = cpt2.maxDate \n\
            INNER JOIN coupons cp \n\
            ON cp.id = cpt.couponId \n\
            AND cpt.type = 3 \n\
            INNER JOIN offers of \n\
            ON of.id = cp.offerId \n\
            AND of.establishmentId = ?;";

            app.db.query(sql, [user.id], function (err, result) {

                if (err) {

                    callback(null);

                    return;
                }

                callback(result);
            },{cache: false});
        },
        getAdviserInfo: function(callback) {
            var user = app.authUser;

            var sql = "SELECT \n\
                adv.id, \n\
                adv.name, \n\
                adv.phone, \n\
                adv.email, \n\
                adv.avatar \n\
            FROM crm_users adv \n\
            INNER JOIN establishments es \n\
            ON es.adviserId = adv.id\
            WHERE es.id = ?;";

            app.db.query(sql, [user.id], function (err, result) {

                if (err) {

                    callback(null);

                    return;
                }

                callback(result);
            },{cache: false});
        },
        getTotalNearbyUsersByCoordinates: function(lat, lng, distance, callback) {
            if (lat == '' || lng == '') {
                callback(0);
                return ;
            }

            var sql = "SELECT count(id) as total FROM users usr WHERE ( \n\
                (ST_DISTANCE(POINT(usr.lat, usr.lng), POINT(" + lat + " , " + lng + "))*60*1.1515*1.609344) >= 0 \n\
            AND (ST_DISTANCE(POINT(usr.lat, usr.lng), POINT(" + lat + " , " + lng + "))*60*1.1515*1.609344) < " + distance + " \n\
            ) AND inactive IS FALSE;";

            app.db.query(sql, function (err, result) {

                if (err) {

                    callback(0);

                    return;
                }

                callback(result[0]);
            },{cache: false});
        }
    }
};