'use strict';

module.exports = function (app) {

    return {
        getByCode: function (code, callback) {

            var user = app.authUser;

            var sql = "SELECT cp.id, cp.code, cp.paymentMethod, cp.quantity, \n\
            (if(of.discount > 0 ,(of.price - (of.price * (of.discount / 100))), of.price) * cp.quantity) new_value, \n\
            cp.dateCreated, \n\
            of.price * cp.quantity as old_value, \n\
            of.description as offer_description, \n\
            of.id offer_id, of.name offer_name, of.couponExpireDate dateFinish, \n\
            (SELECT image FROM offer_images WHERE offerId = of.id AND `order` = 1 LIMIT 1) image, \n\
            us.name as user_name, \n\
            us.image as user_photo, \n\
            cp.ownerUserId as user_id, \n\
            IFNULL((now() - of.couponExpireDate > 0), false) expired, \n\
            (SELECT type FROM coupon_transactions WHERE couponId = cp.id ORDER BY id DESC LIMIT 1) lastType, \n\
            IFNULL((SELECT (amount * (-1)) amount FROM user_financial_transactions WHERE couponTransactionId = cpt.id LIMIT 1), 0) payByCreditsAmount, \n\
            IFNULL((SELECT amount FROM coupon_establishment_payments WHERE couponTransactionId = cpt.id LIMIT 1), 0) payByDirectEstablishmentAmount, \n\
            IFNULL((SELECT amount FROM credit_card_payments WHERE couponTransactionId = cpt.id LIMIT 1), 0) payByCreditCardAmount, \n\
            (SELECT SUM(cp.quantity) as bought \n\
            FROM coupons cp \n\
            INNER JOIN coupon_transactions cpt \n\
            ON (SELECT id \n\
                FROM coupon_transactions \n\
                WHERE couponId = cp.id \n\
                ORDER BY id DESC \n\
                LIMIT 1) = cpt.id \n\
            WHERE cp.offerId = of.id \n\
            AND cpt.type IN (1,2,3) \n\
            ) bought, \n\
            (SELECT SUM(cp.quantity) as redeemed \n\
            FROM coupons cp \n\
            INNER JOIN coupon_transactions cpt \n\
            ON (SELECT id \n\
                FROM coupon_transactions \n\
                WHERE couponId = cp.id \n\
                ORDER BY id DESC \n\
                LIMIT 1) = cpt.id  \n\
            WHERE cp.offerId = of.id \n\
            AND cpt.type = 3 \n\
            ) redeemed \n\
            FROM coupons cp \n\
            INNER JOIN offers of ON cp.offerId = of.id \n\
            INNER JOIN users us \n\
            ON cp.ownerUserId = us.id \n\
            LEFT JOIN coupon_transactions cpt ON cpt.id = (SELECT id FROM coupon_transactions WHERE couponId = cp.id LIMIT 1) \n\
            WHERE cp.code = ? \n\
            AND of.establishmentId = ? \n\
            LIMIT 1;";

            app.db.query(sql, [code, user.id], function (err, result) {

                if (err) {
                    console.log(err);

                    callback({"status":0});

                    return;
                }

                callback(result);
            },{cache: false});
        },
        getInfoByCode: function(code, callback) {
            var user = app.authUser;

            var sql = "SELECT cp.id, cp.code, cp.paymentMethod, cp.quantity, \n\
            (if(of.discount > 0 ,(of.price - (of.price * (of.discount / 100))), of.price) * cp.quantity) new_value, \n\
            of.price * cp.quantity as old_value, \n\
            of.description as offer_description, \n\
            of.id offer_id, of.name offer_name, of.couponExpireDate dateFinish, \n\
            (SELECT image FROM offer_images WHERE offerId = of.id AND `order` = 1 LIMIT 1) image, \n\
            us.name as user_name, \n\
            us.image as user_photo, \n\
            cp.ownerUserId as user_id, \n\
            IFNULL((now() - of.couponExpireDate > 0), false) expired, \n\
            (SELECT type FROM coupon_transactions WHERE couponId = cp.id ORDER BY id DESC LIMIT 1) lastType, \n\
            IFNULL((SELECT (amount * (-1)) amount FROM user_financial_transactions WHERE couponTransactionId = cpt.id LIMIT 1), 0) payByCreditsAmount, \n\
            IFNULL((SELECT amount FROM coupon_establishment_payments WHERE couponTransactionId = cpt.id LIMIT 1), 0) payByDirectEstablishmentAmount, \n\
            IFNULL((SELECT amount FROM credit_card_payments WHERE couponTransactionId = cpt.id LIMIT 1), 0) payByCreditCardAmount \n\
            FROM coupons cp \n\
            INNER JOIN offers of ON cp.offerId = of.id \n\
            INNER JOIN users us \n\
            ON cp.ownerUserId = us.id \n\
            LEFT JOIN coupon_transactions cpt ON cpt.id = (SELECT id FROM coupon_transactions WHERE couponId = cp.id LIMIT 1) \n\
            WHERE cp.code = ? \n\
            AND of.establishmentId = ? \n\
            LIMIT 1;";

            app.db.query(sql, [code, user.id], function (err, result) {
                if (err) {
                    console.log(err);

                    callback({"status":0});

                    return;
                }

                callback(result);
            },{cache: false});
        },
        getImagesByOfferId: function(offer, callback) {
            var user = app.authUser;

            var sql = "SELECT of.id, oi.order, oi.image \n\
            FROM offers of \n\
            INNER JOIN offer_images oi \n\
            ON oi.offerId = of.id \n\
            WHERE of.id = ? \n\
            AND of.establishmentId = ?;";

            app.db.query(sql, [offer, user.id], function (err, result) {

                if (err) {
                    console.log(err);

                    callback([]);

                    return;
                }

                callback(result);
            },{cache: false});
        },
        getObservationsByOfferId: function(offer, callback) {
            var user = app.authUser;

            var sql = "SELECT of.id, oo.title, oo.description \n\
            FROM offers of \n\
            INNER JOIN offer_observations oo \n\
            ON of.id = oo.offerId \n\
            WHERE of.id = ? \n\
            AND of.establishmentId = ?;";

            app.db.query(sql, [offer, user.id], function (err, result) {

                if (err) {
                    console.log(err);

                    callback([]);

                    return;
                }

                callback(result);
            },{cache: false});
        },
        getCouponStatus: function(code, callback) {
            var user = app.authUser;
            var sql = " \
            SELECT cp.id, cpt.type, cp.ownerUserId as user, establishmentId as store \n\
            FROM coupons cp \n\
            INNER JOIN offers of \n\
            ON cp.offerId = of.id \n\
            INNER JOIN coupon_transactions cpt \n\
            ON cpt.couponId = cp.id \n\
            WHERE code = ? \n\
            AND of.establishmentId = ? \n\
            ORDER BY cpt.date DESC \n\
            LIMIT 1;";

            app.db.query(sql, [code, user.id], function (err, result) {

                if (err) {
                    console.log(err);

                    callback(null);

                    return;
                }

                callback(result);
            },{cache: false});
        },
        createRedeemTransactionById: function(id, callback) {
            var sql = 'INSERT INTO coupon_transactions (couponId, type, date, observation)' +
                'VALUES (?,?, now(),?)';

            var observation = 'Coupom was redeemed';

            app.db.query(sql, [id, 3, observation], function (err, result) {

                if (err) {
                    console.log(err);

                    callback(null);

                    return;
                }

                callback(result);
            }, {cache: false});
        }
    }
};