'use strict';

module.exports = function (app) {

    return {
        getUserByToken: function (token, callback) {

            if (token === undefined) {

                callback(null);

                return;
            }


            var sql = "SELECT \n\
            store.id, \n\
                store.name, \n\
                store.lat, \n\
                store.lng, \n\
                store.currency, \n\
                user.name as username, \n\
                user.email, \n\
                user.accessToken \n\
            FROM \n\
            market_users user \n\
            INNER JOIN \n\
            establishments store ON user.establishmentId = store.id \n\
            WHERE \n\
            accessToken = ? \n\
            LIMIT 1;";

            app.db.query(sql, [token], function (err, result) {

                var user = null;
                if (err == null && result.length) {
                    user = result[0];
                } else {

                    app.db.delKey(sql, [token]);
                }

                callback(user);

            }, {TTL: 600});
        },
        setUserToken: function (userId) {
            var token = app.helpers.md5Encrypt(userId + '-' + Date.now() + '-' + Math.random());

            app.db.query("UPDATE market_users SET accessToken = ? WHERE id = ?", [token, userId], function (err, result) {});

            return token;
        }
    }
};