# PROJECT CONFIG #

## INSTALL NODEJS ##

`curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -`

`sudo apt-get install -y nodejs`

`sudo nano /etc/environment`

`export NODE_ENV=production`

# INSTALL FOREVER SERVICE #

`sudo npm install -g forever-service`

`sudo npm install -g forever`

`sudo NODE_ENV=production forever start forever.json`

# APACHE PROXY #

`sudo a2enmod proxy_http`

`ProxyPass /v3 http://127.0.0.1:8081/v3`