'use strict';

module.exports = function (app) {

    var db = require('mysql-cache');

    var config = require('../config/database/' + app.env);

    db.init({
        host: config.host,
        user: config.user,
        password: config.password,
        database: config.database,
        TTL: 0, 				// Time To Live for a cache key in seconds (0 = infinite)
        connectionLimit: 500, 	// Mysql connection pool limit (increase value if you are having problems)
        verbose: app.env != 'production', 			// Do you want console.log's about what the program is doing?
        caching: true 			// Do you want to use SELECT SQL caching?
    });

    return db;
};