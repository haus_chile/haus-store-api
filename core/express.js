// set the server up
var express         = require('express'),
    app             = express(),
    router          = express.Router(),
    compression     = require('compression'),
    bodyParser      = require('body-parser'),
    apiConfig       = require('../config/api'),
    bearerToken     = require('express-bearer-token'),
    authorization   = require('../middlewares/authorization')
    cors            = require('../middlewares/cors');

/**
 * Middlewares
 */
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bearerToken());
app.use(compression());

// CORS Middleware
app.use(cors());

// json custom
app.set('json spaces', apiConfig.JSON_SPACES);

/**
 * Global Variables
 */
// environment
app.env = app.get('env') || 'production';

/**
 * Database
 */
app.db = require('./db')(app);

/**
 * Mobile Url API
 */
app.mobileApi = require('../config/mobile-api/' + app.env);
/**
 * Helpers
 */
app.helpers = require('../components/helpers');

/**
 * Translator
 * @see https://github.com/thestonefox/node-translate
 */
app.i18n = function () {

    var i18n = require('node-translate');

    var locales = {
        'en': require('../locales/en'),
        'es': require('../locales/es'),
        'pt': require('../locales/pt')
    };

    i18n.requireLocales(locales);

    i18n.setLang = function (lang) {

        if (Object.keys(locales).indexOf(lang) < 0) {
            lang = 'es';
        }

        i18n.setLocale(lang);
    };

    return i18n;
};

app.use(authorization(app));

/**
 * Controllers
 */
require('../controllers/default')(app, router);
require('../controllers/users')(app, router);
require('../controllers/coupons')(app, router);

/**
 * mount routes
 */
app.use('/' + apiConfig.VERSION, router);

/**
 * Listen on provided port, on all network interfaces.
 */
var port = apiConfig.PORT || '3000';

app.listen(port, function (error) {

    console.log('Listening on ' + port);
});

process.on("uncaughtException", function(err) {

    console.error((new Date()).toUTCString() + " uncaughtException: " + err.message);

    if(process.env.NODE_ENV != "development") {

        process.exit(1);

    } else {
        //console.error(err.stack);
        process.exit(1);
    }
});