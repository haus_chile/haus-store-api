'use strict';

module.exports = authorization;

function authorization(app) {

    var auth = {
        enabled: true,
        publicRoutes: [
            '/v1/users/login',
            '/v1/users/remember'
        ],
        validate: function (token, url) {

            if (!this.enabled || this.publicRoutes.indexOf(url) != -1) {
                return true;
            }

            var userAuth = require('../services/userAuthorization')(app);

            var authUser;

            userAuth.getUserByToken(token, function (user) {

                authUser = user;
            });

            while (authUser === undefined) {
                app.helpers.deasync.sleep(100);
            }

            if (authUser != null) {

                app.authUser = authUser;

                return true;
            }

            return false;
        }
    };

    return function (req, res, next) {

        var validate = auth.validate(req.token, req.originalUrl);

        if (!validate) {

            res.status(401).send({"response": "token invalid or missing", "code": 401});

            return false;
        }

        next();
    };
};