'use strict';

module.exports = cors;

function cors() {

    return function (req, res, next) {

        res.type('application/json');

        res.header('Access-Control-Allow-Credentials', true);
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization');
        res.header('Cache-Control', 'no-store, no-cache, must-revalidate');
        res.header('Cache-Control', 'post-check=0, pre-check=0');
        res.header('Pragma', 'no-cache');
        res.header('Expires', 'Sat, 26 Jul 1997 05:00:00 GMT');

        if ('OPTIONS' === req.method) {
            res.status(200).end();
        } else {
            next();
        }
    };
};