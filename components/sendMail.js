'use strict';

var nodemailer = require('nodemailer');

module.exports = function (app) {


    return {
        fromName: 'El equipo Haus',
        fromMail: 'hola@haus-app.com',
        unsubVerification: function (email, callback) {

            var sql = "SELECT ums.unsubscribe FROM users usr INNER JOIN user_mail_settings ums ON ums.userId = usr.id WHERE usr.email = ? LIMIT 1;";

            app.db.query(sql, [email], function (err, result) {

                if (result != null && result.length && result[0].unsubscribe == 1) {

                    callback(true);

                    return;
                }

                callback(false);

            }, {cache: false});

        },
        sendHtml: function (subject, toEmail, template, params) {

            var fs = require('fs');
            var self = this;

            fs.readFile(__dirname + '/../var/mail/'+template+'.html', 'utf8', function (err, html) {

                if (Object.keys(params).length) {

                    Object.keys(params).forEach(function (key) {

                        html = html.replace('{'+key+'}', params[key]);

                    });
                }

                self.send(subject, toEmail, html);

            });
        },
        send: function (subject, toEmail, html, text) {

            var self = this;
            var resp;


            self.unsubVerification(toEmail, function (isUnsub) {

                if (isUnsub) {
                    return;
                }

                var transporter = nodemailer.createTransport({
                    host: 'smtp.mandrillapp.com',
                    port: 587,
                    secure: false, // use SSL
                    auth: {
                        user: 'Haus',
                        pass: 'LwCz0_OLFArI3doRVgsM1g'
                    }
                });

                try {

                    var mailOptions = {
                        from: '"'+self.fromName+'" <'+self.fromMail+'>', // sender address
                        to: toEmail,
                        subject: subject,
                        text: text,
                        html: html
                    };

                    transporter.sendMail(mailOptions, function(error, info){
                        if(error){
                            return console.log(error);
                        }
                        console.log('Message sent: ' + info.response);
                    });

                } catch (e) {

                    console.log("Error on sendmail");
                }
            });
        }
    };
};
