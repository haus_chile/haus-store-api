'use strict';

module.exports = {
    deasync: require('deasync'),
    parseJson: function (jsonString) {

        if (typeof jsonString == 'object') {
            return jsonString;
        }

        var json = {};

        try {

            json = JSON.parse(jsonString);

        } catch (error) {

            json = {};
            console.log(error.message);
        }

        return json;
    },
    base64ToFile: function (file, base64Data) {

        base64Data = base64Data.replace(/^data:image\/png;base64,/, "");

        var path = require('path');
        var fs = require("fs");

        fs.writeFile(path.join(__dirname, "/../" + file), base64Data, 'base64', function(err) {

            if (err != null) {
                console.log(err);
            }
        });
    },
    deleteFile: function (file) {

        var fs = require("fs");

        file = __dirname + "/../" + file;

        fs.exists(file, function (exists) {

            if (exists) {

                fs.unlinkSync(file);
            }

        });
    },
    getFirstWord: function(string) {
      if (string) {
          return string.split(" ",1)[0];
      }
    },
    shuffleString: function(string) {
        var parts = string.split('');
        for (var i = parts.length; i > 0;) {
            var random = parseInt(Math.random() * i);
            var temp = parts[--i];
            parts[i] = parts[random];
            parts[random] = temp;
        }
        return parts.join('');
    },
    md5Encrypt: function (data) {

        var crypto = require('crypto');

        return crypto.createHash('md5').update(data).digest("hex");
    },
    format: function (value, currency) {

        var currencyFormatter = require('./currencyFormatter');

        value = (currencyFormatter.format(value, {symbol: '', code: currency})).trim();

        return currency + ' ' + value;
    },
    scape: function (vl) {

        var str = require('string');

        return str(vl).decodeHTMLEntities().s;
    }
};